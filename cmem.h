
#ifndef _CMEM_H_
#define _CMEM_H_

#include <stdio.h>

#ifdef __cplusplus
extern "C" {
#endif

typedef struct cmem_stat_t cmem_stat_t;

#pragma pack(push, 1)
struct cmem_stat_t
{
    long allocated_count;
    long allocated_size;
    char * from_last_free_str;
};
#pragma pack(pop)

cmem_stat_t * cmem_stat_create();
void cmem_free_all(cmem_stat_t * stat);
void cmem_stat_delete(cmem_stat_t ** stat);
void * cmem_realloc(cmem_stat_t * stat, void * p, size_t size, char * text);
void * cmem_malloc(cmem_stat_t * stat, size_t size, char * text);
#define cmem_free(stat, pointer) cmem_free_(stat, (void**)&(pointer))
void cmem_free_(cmem_stat_t * stat, void ** p);

#ifdef __cplusplus
}
#endif

#endif //_CMEM_H_
