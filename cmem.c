
#include "cmem.h"

#include <string.h>
#include <stdlib.h>


#define container_of(ptr, type, member) ({ \
    const typeof( ((type *)0)->member ) *__mptr = (ptr); \
    (type *)( (char *)__mptr - (size_t)&((type *)0)->member );})

typedef struct cmem_stat_parent_t cmem_stat_parent_t;
typedef struct cmem_descriptor_list_t cmem_descriptor_list_t;

#pragma pack(push, 1)
struct cmem_stat_parent_t
{
    cmem_descriptor_list_t * first_descriptor;
    cmem_descriptor_list_t * last_descriptor;
    cmem_stat_t child;
};
#pragma pack(pop)

struct cmem_descriptor_list_t
{
    cmem_descriptor_list_t * prev;
    cmem_descriptor_list_t * next;
    void * p;
    size_t size;
    char * text;
};

//-------------------------------------------------------------------

cmem_stat_t * cmem_stat_create()
{
    cmem_stat_parent_t * stat_parent = malloc(sizeof(cmem_stat_parent_t));
    memset(stat_parent, 0, sizeof(cmem_stat_parent_t));
    return &stat_parent->child;
}

cmem_stat_parent_t * cmem_get_stat_parent(cmem_stat_t * stat)
{
    return container_of(stat, cmem_stat_parent_t, child);
}

void cmem_free_all(cmem_stat_t * stat)
{
    if (stat == NULL) return;

    cmem_stat_parent_t * stat_parent = cmem_get_stat_parent(stat);
    cmem_descriptor_list_t * next, * now;
    now = stat_parent->first_descriptor;
    while (now)
    {
        if (now->text) free(now->text);
        if (now->p) free(now->p);
        next = now->next;
        free(now);
        now = next;
    }
    if (stat->from_last_free_str) free(stat->from_last_free_str);
}

void cmem_stat_delete(cmem_stat_t ** stat)
{
    if (*stat == NULL) return;

    cmem_free_all(*stat);
    cmem_stat_parent_t * stat_parent = cmem_get_stat_parent(*stat);
    memset(stat_parent, 0, sizeof(cmem_stat_parent_t));
    free(stat_parent);
    *stat = NULL;
}

cmem_descriptor_list_t * cmem_find_descriptor(cmem_stat_parent_t * stat_parent, void * p)
{
    cmem_descriptor_list_t * pdesc = stat_parent->first_descriptor;
    while(pdesc)
    {
        if (p == pdesc->p)
        {
            return pdesc;
        }
        pdesc = pdesc->next;
    }
    return NULL;
}

void * cmem_realloc(cmem_stat_t * stat, void * p, size_t size, char * text)
{
    if (stat == NULL) return NULL;

    cmem_stat_parent_t * stat_parent = cmem_get_stat_parent(stat);
    cmem_descriptor_list_t * desc = cmem_find_descriptor(stat_parent, p);
    size_t size_old = 0;

    if (desc)
    {
        size_old = desc->size;
        desc->size = size;
    }
    else
    {
        stat->allocated_count++;

        desc = malloc(sizeof(cmem_descriptor_list_t));
        if (stat_parent->first_descriptor == NULL) stat_parent->first_descriptor = desc;
        if (stat_parent->last_descriptor) stat_parent->last_descriptor->next = desc;
        desc->prev = stat_parent->last_descriptor;
        stat_parent->last_descriptor = desc;

        desc->next = NULL;
        desc->size = size;
        if (text)
            desc->text = strdup(text);
        else
            desc->text = NULL;
    }
    long size_diff = (long)size - (long)size_old;
    stat->allocated_size += size_diff;

    char * ret = realloc(p, size);
    desc->p = ret;

    if (size_diff > 0)
        memset(ret + size_old, 0, (size_t)size_diff);

    return ret;
}

void * cmem_malloc(cmem_stat_t * stat, size_t size, char * text)
{
    return cmem_realloc(stat, NULL, size, text);
}

void cmem_free_(cmem_stat_t * stat, void ** p)
{
    if (stat == NULL) return;

    cmem_stat_parent_t * stat_parent = cmem_get_stat_parent(stat);
    cmem_descriptor_list_t * desc = cmem_find_descriptor(stat_parent, *p);
    if (desc)
    {
        stat->allocated_count--;
        stat->allocated_size -= desc->size;
        if (stat->from_last_free_str)
            free(stat->from_last_free_str);
        stat->from_last_free_str = desc->text;
        desc->text = NULL;
        free(*p);
        if (desc->prev) desc->prev->next = desc->next;
        if (desc->next) desc->next->prev = desc->prev;
        if (stat_parent->first_descriptor == desc) stat_parent->first_descriptor = desc->next;
        if (stat_parent->last_descriptor == desc) stat_parent->last_descriptor = desc->prev;
        free(desc);
    }
    *p = NULL;
}
