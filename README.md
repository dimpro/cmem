
# cmem
this is library for controlling allocating memory

##### how to use
example:
``` C
#include "cmem/cmem.h"

#include <stdio.h>
#include <stdlib.h>

void print_stat(cmem_stat_t * s)
{
  printf("allocated count: %ld allocated size: %ld text from last free: %s\n",
      s->allocated_count, s->allocated_size, s->from_last_free_str);
}

int main()
{
  //--- first example ---
  //you must use cmem_stat_create() for allocate a memory for info struct
  cmem_stat_t * cmem_stat = cmem_stat_create();
  //use this struct for any cmem functions

  //lets allocate some memory
  print_stat(cmem_stat);
  int * v1 = cmem_malloc(cmem_stat, sizeof(int), "allocated for int");
  print_stat(cmem_stat);
  float * v2 = cmem_malloc(cmem_stat, sizeof(int), "allocated for float");
  print_stat(cmem_stat);
  char * v3 = cmem_malloc(cmem_stat, 90, "allocated for string");
  print_stat(cmem_stat);
  v3 = cmem_realloc(cmem_stat, v3, 110, "reallocated memory for string");
  print_stat(cmem_stat);

  cmem_free(cmem_stat, v2);
  print_stat(cmem_stat);
  cmem_free(cmem_stat, v3);
  print_stat(cmem_stat);
  cmem_free(cmem_stat, v1);
  print_stat(cmem_stat);

  //you can just free all allocated memory before
  cmem_free_all(cmem_stat);
  //in the end you must delete stat
  cmem_stat_delete(&cmem_stat);

  return 0;
}
```
output:

allocated count: 0 allocated size: 0 text from last free: (null)
allocated count: 1 allocated size: 4 text from last free: (null)
allocated count: 2 allocated size: 8 text from last free: (null)
allocated count: 3 allocated size: 98 text from last free: (null)
allocated count: 3 allocated size: 118 text from last free: (null)
allocated count: 2 allocated size: 114 text from last free: allocated for float
allocated count: 1 allocated size: 4 text from last free: allocated for string
allocated count: 0 allocated size: 0 text from last free: allocated for int

